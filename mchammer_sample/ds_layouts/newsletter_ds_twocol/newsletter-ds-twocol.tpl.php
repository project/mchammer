<?php

/**
 * @file
 * Display Suite table template: top, bottom, left, right.
 */
?>
<table class="ds-table <?php print $view_mode; ?>" width="100%" cellspacing="0" cellpadding="0" border="0">
<?php if ($top): ?>
<tr>
  <td colspan="2"><?php print $top; ?></td>
</tr>
<?php endif; ?>
<?php if ($left || $right): ?>
<tr>
  <td style="padding-right:10px;" valign="top">
    <?php print $left ?>
  </td>
  <td valign="top">
    <?php print $right ?>
  </td>
</tr>
<?php endif; ?>
<?php if ($bottom): ?>
<tr>
  <td colspan="2"><?php print $bottom; ?></td>
</tr>
<?php endif; ?>
</table>