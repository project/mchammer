<?php
/**
 * Example of mchmammer plugin defintion.
 * Required keys:
 *   - title
 *   - theme
 *   - regions
 *   - module => mchammer
 *   - html-mchammer
 *
 * Variables key:
 *   - title => title from the form field
 *   - type => form api type of field
 *   - imce => set to true, if you want an imce link below this text field.
 *             People can upload images this way. (if imce is enabled)
 */
$plugin = array(
  'title' => t('Example: Modern'),
  'category' => t('Mail templates'),
  'icon' => 'sample.png',
  'theme' => 'modern',
  'regions' => array(
    'left' => t('Left column'),
    'middle' => t('Middle column'),
  ),
  'variables' => array(
    'sidebar_banner' => array(
      'title' => t('Banner in sidebar (image will not be autoscaled)'),
      'type' => 'textfield',
      'imce' => TRUE
    ),
    'about' => array(
      'title' => t('Short intro of current newsletter'),
      'type' => 'text_format'
    ),
  ),
  'module' => 'mchammer', // mchammer as module is required
  'html-mchammer' => 'html-mchammer-modern', // html.tpl.php for mchammer
);
