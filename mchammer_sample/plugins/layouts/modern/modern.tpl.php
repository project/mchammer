 <?php
/**
 * @file
 * Example mailtemplate: Modern template from campaign monitor.
 */
global $base_url;
$image_path = $base_url . '/' . drupal_get_path('module', 'mchammer_sample') . '/plugins/layouts/modern/images';
?>

<table cellpadding="0" cellspacing="0" border="0" align="center" width="800" style="font-family: Helvetica, Arial, sans-serif; background: #fff url('<?php print $image_path ?>/bg_table.png') repeat-y;" bgcolor="#fff">
  <tr>
    <td width="150" valign="top" align="left" style="font-family: Helvetica, Arial, sans-serif;" bgcolor="#fff" class="sidebar">

      <?php if (!empty($about)): ?>
      <h2 class="pane-title"><?php print t('This month')?></h2>
      <?php print check_markup($about['value'], $about['format']); ?>
      <div class="panel-separator"></div>
      <?php endif; ?>

      <?php print $content['left']; ?>

      <?php if (!empty($sidebar_banner)): ?>
      <div class="panel-separator"></div>
      <?php print theme('image', array('path' => $sidebar_banner, 'width' => 150)); ?>
      <?php endif; ?>

    </td>
    <td width="520" valign="top" align="left" style="font-family: Helvetica, Arial, sans-serif;" class="content">
      <?php print $content['middle']; ?>
    </td>
  </tr>
  <tr>
    <td width="800" align="left" style="padding: font-size: 0; line-height: 0; height: 3px;" height="3" colspan="2"><img src="<?php print $image_path ?>/bg_bottom.png" alt="header bg"></td>
  </tr>
</table><!-- body -->