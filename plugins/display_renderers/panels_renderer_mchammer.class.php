<?php

/**
 * Renderer class for all MCHammer behavior.
 */
class panels_renderer_mchammer extends panels_renderer_editor {

  /**
   * Get the links for a panel display.
   *
   */
  function get_display_links() {
    return;
  }

  /**
   * Render the links to display when editing a region.
   */
  function get_region_links($region_id) {

    $links = array();
    $links[] = array(
      'title' => t('Add content'),
      'href' => $this->get_url('select-content', $region_id),
      'attributes' => array(
        'class' => array('ctools-use-modal'),
      ),
    );

    // Todo: improve this check.
    if (preg_match("|mchammer:template:|", $this->display->cache_key)) {
	    $links[] = array(
	      'title' => '<hr />',
	      'html' => TRUE,
	    );

	    $links[] = array(
	      'title' => t('Configure region'),
	      'href' => $this->get_url('configure-region', $region_id),
	      'attributes' => array(
	        'class' => array('ctools-use-modal'),
	      ),
	    );
    }

    return theme('ctools_dropdown', array('title' => theme('image', array('path' => ctools_image_path('icon-addcontent.png', 'panels'))), 'links' => $links, 'image' => TRUE, 'class' => 'pane-add-link panels-region-links-' . $region_id));

  }

  /**
   * AJAX command to add a content table for all content.
   */
  function ajax_add_content_table($region_id = NULL, $category = NULL) {

    //panels_edit_cache_set($this->cache);

    $this->commands[] = ctools_modal_command_dismiss();

    $this->command_update_region_links($region_id);

  }

  /**
   * AJAX command to present a dialog with a list of available content.
   */
  function ajax_configure_region($region_id = NULL, $category = NULL) {

    $info = $this->get_view_modes($region_id);
    $view_modes = $info[0];
    $conf = &$info[1];

    $title = t('Configure !region', array('!region' => $this->plugins['layout']['regions'][$region_id]));

    $form_state = array(
      'display' => &$this->display,
      'type' => 'region',
      'region_id' => $region_id,
      'conf' => &$conf,
      'view_modes' => $view_modes,
      'ajax' => TRUE,
      'title' => $title,
      'url' => url($this->get_url('configure-region', $region_id), array('absolute' => TRUE)),
      'renderer' => &$this,
    );

    $output = ctools_modal_form_wrapper('panels_region_configuration_form', $form_state);
    if (empty($form_state['executed'])) {
      $this->commands = $output;
      return;
    }

    if (isset($this->cache->view_modes)) {
      unset($this->cache->view_modes);
    }

    // $conf was a reference so it should just modify.
    panels_edit_cache_set($this->cache);

    $this->commands[] = ctools_modal_command_dismiss();

    $this->command_update_region_links($region_id);

  }

  /**
   * Get the appropriate view modes from the panel in the cache.
   */
  function get_view_modes($region_id = '') {
    $view_modes = !empty($this->display->panel_settings['view_modes_settings'][$region_id]['view_modes']) ? $this->display->panel_settings['view_modes_settings'][$region_id]['view_modes'] : variable_get('mchammer_view_modes', array());
    $view_modes = array_filter($view_modes);
    // Set up our $conf reference.
    $conf = &$this->display->panel_settings['view_modes_settings'][$region_id];

    return array($view_modes, &$conf);
  }

  /**
   * Render the links to display when editing a pane.
   */
  function get_pane_links($pane, $content_type) {
    $links = array();

    if (!empty($pane->shown)) {
      $links[] = array(
        'title' => t('Disable this pane'),
        'href' => $this->get_url('hide', $pane->pid),
        'attributes' => array('class' => array('use-ajax')),
      );
    }
    else {
      $links[] = array(
        'title' => t('Enable this pane'),
        'href' => $this->get_url('show', $pane->pid),
        'attributes' => array('class' => array('use-ajax')),
      );
    }

    $subtype = ctools_content_get_subtype($content_type, $pane->subtype);

    if (ctools_content_editable($content_type, $subtype, $pane->configuration)) {
      $links[] = array(
        'title' => isset($content_type['edit text']) ? $content_type['edit text'] : t('Settings'),
        'href' => $this->get_url('edit-pane', $pane->pid),
        'attributes' => array('class' => array('ctools-use-modal')),
      );
    }

    $links[] = array(
      'title' => t('Remove'),
      'href' => '#',
      'attributes' => array(
        'class' => array('pane-delete'),
        'id' => "pane-delete-panel-pane-$pane->pid",
      ),
    );

    if (isset($pane->configuration['source'])) {
      $links[] = array(
        'title' => t('Rerender source'),
        'href' => 'mchammer/nojs/rerender/' . $this->display->cache_key . '/' . $this->mail_template_name . '/' . $pane->configuration['source'],
        'attributes' => array(
          'class' => array('ctools-use-modal'),
          'id' => "pane-revert-source-pane-$pane->pid",
        ),
      );
    }

    return theme('ctools_dropdown', array('title' => theme('image', array('path' => ctools_image_path('icon-configure.png', 'panels'))), 'links' => $links, 'image' => TRUE));

  }

}

/**
 * Region settings form
 */
function panels_region_configuration_form($form, &$form_state) {
  $display = &$form_state['display'];
  $conf = &$form_state['conf'];
  $region_id = $form_state['region_id'];
  $enabled_view_modes = $form_state['view_modes'];

  $form['#action'] = $form_state['url'];

  $entity_info = entity_get_info('node');
  $options = array();
  foreach ($entity_info['view modes'] as $name => $data) {
    $options[$name] = $data['label'];
  }

	$form['settings']['view_modes'] = array(
	  '#title' => t('Available view modes for Newsletters'),
	  '#type' => 'checkboxes',
	  '#options' => $options,
	  '#default_value' => $enabled_view_modes,
	  '#description' => t('Enable the view modes that can be used in derived Newsletters.'),
	);

  $form['settings']['#tree'] = TRUE;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Allows panel styles to validate their style settings.
 */
function panels_region_configuration_form_submit($form, &$form_state) {
  $form_state['conf'] = $form_state['values']['settings'];
}