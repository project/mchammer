 <?php
/**
 * @file
 * Template for a 2 column mail layout.
 */
?>
<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">

  <tr>
    <td width="640" valign="top" class="left-region"><?php print $content['left']; ?></td>
    <td width="160" valign="top" class="right-region" style="padding: 0 20px;">
      <?php print $content['right']; ?>
    </td>
  </tr>

</table>