<?php

/**
 * @file
 * Ctools export UI for a McHammer Mail Template.
 */
class mchammer_mail_template_ui extends mchammer_ui {

  protected $cacheKeyPrefix = 'template';
  protected $mchammerType = 'mail-template';

  /**
   * Implements list_build_row().
   */
  function list_build_row($item, &$form_state, $operations) {

    $operations['preview'] = array(
      'href' => 'mail-template/' . $item->name,
      'title' => t('Preview'),
    );

    //admin/structure/mchammer/newsletter/add/basic/newsletter_example_1
    $newsletter_plugin = ctools_get_export_ui('newsletter.export_ui');
    $operations['create_newsletter'] = array(
     'href' => 'admin/structure/mchammer/newsletter/add/' . $item->name . '/create-newsletter',
     // 'href' => ctools_export_ui_plugin_menu_path($newsletter_plugin, 'add/basic/%ctools_export_ui', $item->name),
     'title' => t('Create newsletter'),
    );

    // Set up sorting
    $name = $item->{$this->plugin['export']['key']};
    $schema = ctools_export_get_schema($this->plugin['schema']);

    // Note: $item->{$schema['export']['export type string']} should have already been set up by export.inc so
    // we can use it safely.
    switch ($form_state['values']['order']) {
      case 'disabled':
        $this->sorts[$name] = empty($item->disabled) . $name;
        break;
      case 'admin_title':
        $this->sorts[$name] = $item->{$this->plugin['export']['admin_title']};
        break;
      case 'name':
        $this->sorts[$name] = $name;
        break;
      case 'storage':
        $this->sorts[$name] = $item->{$schema['export']['export type string']} . $name;
        break;
    }

    $this->rows[$name]['data'] = array();
    $this->rows[$name]['class'] = !empty($item->disabled) ? array('ctools-export-ui-disabled') : array('ctools-export-ui-enabled');

    // If we have an admin title, make it the first row.
    if (!empty($this->plugin['export']['admin_title'])) {
      $this->rows[$name]['data'][] = array('data' => check_plain($item->{$this->plugin['export']['admin_title']}), 'class' => array('ctools-export-ui-title'));
    }
    $this->rows[$name]['data'][] = array('data' => check_plain($name), 'class' => array('ctools-export-ui-name'));
    $this->rows[$name]['data'][] = array('data' => check_plain($item->{$schema['export']['export type string']}), 'class' => array('ctools-export-ui-storage'));

    $ops = theme('links__ctools_dropbutton', array('links' => $operations, 'attributes' => array('class' => array('links', 'inline'))));

    $this->rows[$name]['data'][] = array('data' => $ops, 'class' => array('ctools-export-ui-operations'));

    // Add an automatic mouseover of the description if one exists.
    if (!empty($this->plugin['export']['admin_description'])) {
      $this->rows[$name]['title'] = $item->{$this->plugin['export']['admin_description']};
    }
  }

  /**
   * Implements edit_form().
   */
  function edit_form(&$form, &$form_state) {

    // Get the basic edit form
    parent::edit_form($form, $form_state);

    $form['title']['#title'] = t('Title');
    $form['title']['#description'] = t('The title for this newsletter template.');

  }

  /**
   * Validate submission of the mail template edit form.
   */
  function edit_form_basic_validate($form, &$form_state) {
    parent::edit_form_validate($form, $form_state);
    // 'add' is a pre-reserved machine name
    if ($form_state['values']['name'] == 'add') {
      form_set_error('name', t("'add' can't be used as a title"));
    }
  }

  /**
   * Step 2 of wizard: Choose a layout.
   */
  function edit_form_layout(&$form, &$form_state) {

    ctools_include('common', 'panels');
    ctools_include('display-layout', 'panels');
    ctools_include('plugins', 'panels');

    // Trigger the module restriction for the allowed layouts.
    $available_layouts = panels_get_layouts();
    $allowed_layouts = new stdClass();
    $allowed_layouts->allowed_layout_settings = array();
    foreach ($available_layouts as $name => $layout) {
      $allowed_layouts->allowed_layout_settings[$name] = $layout['module'] == 'mchammer';
    }

    $form_state['allowed_layouts'] = $allowed_layouts;

    // Make sure there is a display to work with.
    if ($form_state['op'] == 'add' && empty($form_state['item']->display)) {
      $form_state['item']->display = panels_new_display();
    }
    $form_state['item']->display->renderer = 'mchammer';
    $form_state['display'] = &$form_state['item']->display;

    // Tell the Panels form not to display buttons.
    $form_state['no buttons'] = TRUE;

    // Change the #id of the form so the CSS applies properly.
    $form['#id'] = 'panels-choose-layout';
    $form = panels_choose_layout($form, $form_state);

    if ($form_state['op'] == 'edit') {
      $form['buttons']['next']['#value'] = t('Change');
    }

  }

  /**
   * Validate that a layout was chosen.
   */
  function edit_form_layout_validate(&$form, &$form_state) {
    $display = &$form_state['display'];
    if (empty($form_state['values']['layout'])) {
      form_error($form['layout'], t('You must select a layout.'));
    }
    if ($form_state['op'] == 'edit') {
      if ($form_state['values']['layout'] == $display->layout) {
        form_error($form['layout'], t('You must select a different layout if you wish to change layouts.'));
      }
    }
  }

  /**
   * A layout has been selected, set it up.
   */
  function edit_form_layout_submit(&$form, &$form_state) {

    $display = &$form_state['display'];
    if ($form_state['op'] == 'edit') {
      if ($form_state['values']['layout'] != $display->layout) {
        $form_state['item']->temp_layout = $form_state['values']['layout'];
        $form_state['clicked_button']['#next'] = 'move';
      }
    }
    else {
      $form_state['item']->display->layout = $form_state['values']['layout'];
    }

  }

  /**
   * When a layout is changed, the user is given the opportunity to move content.
   */
  function edit_form_move(&$form, &$form_state) {

    $form_state['display'] = &$form_state['item']->display;
    $form_state['layout'] = $form_state['item']->temp_layout;

    ctools_include('common', 'panels');
    ctools_include('display-layout', 'panels');
    ctools_include('plugins', 'panels');

    // Tell the Panels form not to display buttons.
    $form_state['no buttons'] = TRUE;

    // Change the #id of the form so the CSS applies properly.
    $form = panels_change_layout($form, $form_state);

    // This form is outside the normal wizard list, so we need to specify the
    // previous/next forms.
    $form['buttons']['previous']['#next'] = 'layout';
    $form['buttons']['next']['#next'] = 'content';

  }

  /**
   * Save the changed selection of layout.
   */
  function edit_form_move_submit(&$form, &$form_state) {
    panels_change_layout_submit($form, $form_state);
  }

  /**
   * Step 3 of wizard: Choose the content.
   */
  function edit_form_content(&$form, &$form_state) {
    parent::edit_form_content($form, $form_state);
  }

  /**
   * Save the display.
   */
  function edit_form_content_submit(&$form, &$form_state) {

    // Make sure all view mode settings are correct.
    foreach (array_keys($form_state['display']->panels) as $region) {
      if (!isset($form_state['display']->panel_settings['view_modes_settings'][$region])) {
        $form_state['display']->panel_settings['view_modes_settings'][$region]['view_modes'] = array_filter(variable_get('mchammer_view_modes', array()));
      }
    }

    parent::edit_form_content_submit($form, $form_state);

    $form_state['item']->display = $form_state['display'];

  }

  /**
   * Create the newsletter display from a given display name.
   * @param $mailtemplate_name Machine name from the panel display to use.
   * @return A new generated panel display.
   */
  function create_newsletter($mailtemplate_name) {

    ctools_include('content');

    // Load the original mail template
    $template = mchammer_mail_template_load($mailtemplate_name);

    $display = panels_new_display();

    // Inherit the layout.
    $display->layout = $template->display->layout;

    // Inherit the panel_settings.
    $display->panel_settings = $template->display->panel_settings;

    // Add the Master template name to the derived Newsletter.
    $display->panel_settings['template_name'] = $mailtemplate_name;

    // Construct the panes for the new display
    foreach ($template->display->panels as $region) {

      foreach ($region as $pid) {

        $pane = $template->display->content[$pid];

        $extractor = McHammerExtractorFactory::getExtractor($pane->type, $template->display);
        $extractor->setSourcePane($pane);
        $extractor->extract($display);

      }

    }

    return $display;

  }

  /**
   * Revert the newsletter display his panes from a given source.
   * @param $mailtemplate_name Machine name from the panel display to revert to.
   * @param $display_cache_key Panels cache key from newsletter display
   * @param $source_key Key from the source panes to be reverted.
   * @return The reverted display.
   */
  function revert_newsletter_pane($mailtemplate_name, $display_cache_key, $source_key) {

    ctools_include('content');

    // Load the current display from cache
    $cache = panels_edit_cache_get($display_cache_key);
    $display = $cache->display;

    // Load the original mail template
    $template = mchammer_mail_template_load($mailtemplate_name);

    // Remove the panes from the source to revert.
    foreach ($display->content as $key => $content) {
      if ($content->configuration['source'] == $source_key) {
        $pane = $display->content[$key];
        unset($display->panels[$pane->panel][$key]);
        unset($display->content[$key]);
      }
    }

    list(, $pane_key) = explode(':', $source_key);
    list(, $pid) = explode('-', $pane_key);

    $pane = $template->display->content[$pid];

    $extractor = McHammerExtractorFactory::getExtractor($pane->type, $template->display);
    $extractor->setSourcePane($pane);
    $extractor->extract($display);

    $cache->display = $display;

    panels_edit_cache_set($cache);

    return $display;

  }

}