<?php

/**
 * @file
 * Ctools export UI for a McHammer Newsletter.
 */
class mchammer_newsletter_ui extends mchammer_ui {

  private $list = array();
  protected $cacheKeyPrefix = 'newsletter';
  protected $mchammerType = 'newsletter';
  protected $rendererHandler = 'mchammer_newsletter';

  /**
   * Implements ctools_export_ui::init().
   * Create menu entries for extra operations.
   */
  function init($plugin) {

    $plugin['menu']['items']['create-newsletter'] = array();
    $plugin['menu']['items']['create-newsletter']['path'] = 'add/%/create-newsletter';
    $plugin['menu']['items']['create-newsletter']['title'] = 'Create newsletter from template';
    $plugin['menu']['items']['create-newsletter']['page callback'] = 'ctools_export_ui_switcher_page';
    $plugin['menu']['items']['create-newsletter']['page arguments'] = array($plugin['name'], 'create_newsletter', 5);
    $plugin['menu']['items']['create-newsletter']['access callback'] = 'user_access';
    $plugin['menu']['items']['create-newsletter']['access arguments'] = array('create newsletters');
    $plugin['menu']['items']['create-newsletter']['load arguments'] = array($plugin['name']);
    $plugin['menu']['items']['create-newsletter']['type'] = MENU_CALLBACK;

    return parent::init($plugin);

  }

  /**
   * Implements ctools_export_ui::edit_cache_get_key().
   * Figure out what the cache key is for this object.
   */
  function edit_cache_get_key($item, $op) {
    $export_key = $this->plugin['export']['key'];
    return $op == 'edit' ? 'newsletter:' . $item->{$this->plugin['export']['key']} : "newsletter:::$op";
  }

  /**
   * Implements ctools_export_ui::list_build_row().
   * Creates a list of newsletters.
   */
  function list_build_row($item, &$form_state, $operations) {

    $operations['view'] = array(
      'href' => 'newsletter/' . $item->name,
      'title' => t('View'),
    );
    $languages = language_list();
    if (isset($languages[$item->language])) {
      $operations['view']['language'] = $languages[$item->language];
    }

    // Set up sorting.
    $name = $item->{$this->plugin['export']['key']};
    $schema = ctools_export_get_schema($this->plugin['schema']);

    // Note: $item->{$schema['export']['export type string']} should have already been set up by export.inc so
    // we can use it safely.
    switch ($form_state['values']['order']) {
      case 'disabled':
        $this->sorts[$name] = empty($item->disabled) . $name;
        break;
      case 'admin_title':
        $this->sorts[$name] = $item->{$this->plugin['export']['admin_title']};
        break;
      case 'name':
        $this->sorts[$name] = $name;
        break;
      case 'storage':
        $this->sorts[$name] = $item->{$schema['export']['export type string']} . $name;
        break;
    }

    $this->rows[$name]['data'] = array();
    $this->rows[$name]['class'] = !empty($item->disabled) ? array('ctools-export-ui-disabled') : array('ctools-export-ui-enabled');

    // If we have an admin title, make it the first row.
    if (!empty($this->plugin['export']['admin_title'])) {
      $this->rows[$name]['data'][] = array('data' => check_plain($item->{$this->plugin['export']['admin_title']}), 'class' => array('ctools-export-ui-title'));
    }
    $this->rows[$name]['data'][] = array('data' => check_plain($name), 'class' => array('ctools-export-ui-name'));
    $this->rows[$name]['data'][] = array('data' => check_plain($item->{$schema['export']['export type string']}), 'class' => array('ctools-export-ui-storage'));

    $ops = theme('links__ctools_dropbutton', array('links' => $operations, 'attributes' => array('class' => array('links', 'inline'))));

    $this->rows[$name]['data'][] = array('data' => $ops, 'class' => array('ctools-export-ui-operations'));

    // Add an automatic mouseover of the description if one exists.
    if (!empty($this->plugin['export']['admin_description'])) {
      $this->rows[$name]['title'] = $item->{$this->plugin['export']['admin_description']};
    }
  }

  /**
   * Implements ctools_export_ui::add_page().
   */
  function add_page($js, $input, $step = NULL, $template_name = NULL) {

    drupal_set_title($this->get_page_title('add'));

    // If a step not set, they are trying to create a new item. If a step
    // is set, they're in the process of creating an item.
    if (!empty($this->plugin['use wizard']) && !empty($step)) {
      $item = $this->edit_cache_get(NULL, 'add');
    }

    // Instantiate a new mail template plugin.
    if (empty($item)) {
      $item = ctools_export_crud_new($this->plugin['schema']);
    }

    $form_state = array(
      'plugin' => $this->plugin,
      'object' => &$this,
      'ajax' => $js,
      'item' => $item,
      'op' => 'add',
      'form type' => 'add',
      'rerender' => TRUE,
      //'no_redirect' => TRUE,
      'no_redirect' => FALSE,
      'step' => $step,
      // Store these in case additional args are needed.
      'function args' => func_get_args(),
      'template_name' => $template_name,
    );

    $output = $this->edit_execute_form($form_state);
    if (!empty($form_state['executed'])) {
      $export_key = $this->plugin['export']['key'];
      drupal_goto(str_replace('%ctools_export_ui', $form_state['item']->{$export_key}, $this->plugin['redirect']['add']));
    }

    return $output;

  }

  /**
   * Implements ctools_export_ui::edit_page().
   * Main entry point to edit an item.
   */
  function edit_page($js, $input, $item, $step = NULL) {

    drupal_set_title($this->get_page_title('edit', $item));

    // Check to see if there is a cached item to get if we're using the wizard.
    if (!empty($this->plugin['use wizard'])) {
      $cached = $this->edit_cache_get($item, 'edit');
      if (!empty($cached)) {
        $item = $cached;
      }
    }

    $form_state = array(
        'plugin' => $this->plugin,
        'object' => &$this,
        'ajax' => $js,
        'item' => $item,
        'op' => 'edit',
        'form type' => 'edit',
        'rerender' => TRUE,
        'no_redirect' => TRUE,
        'step' => $step,
        // Store these in case additional args are needed.
        'function args' => func_get_args(),
    );

    $output = $this->edit_execute_form($form_state);
    if (!empty($form_state['executed'])) {
      $export_key = $this->plugin['export']['key'];
      // Override the default redirect to the list to always show what is shown in the newsletter.
      // $redirect = $this->plugin['redirect']['edit'];
      $redirect = ctools_export_ui_plugin_menu_path($this->plugin, 'edit', $form_state['item']->{$export_key}) . '/content';
      drupal_goto(str_replace('%ctools_export_ui', $form_state['item']->{$export_key}, $redirect));
    }

    return $output;

  }

  /**
   * Page callback to add a newsletter from template
   */
  function create_newsletter_page($js, $input, $template_name, $step = NULL) {
    return $this->add_page($js, $input, $step, $template_name);
  }

  /**
   * Implements ctools_export_ui::edit_form().
   * Edit form
   */
  function edit_form(&$form, &$form_state) {

    $this->get_mail_templates();

    if (isset($form_state['template_name'])) {

      // Bail out if the template does not exist.
      if (!isset($this->list[$form_state['template_name']])) {
        drupal_not_found();
        exit;
      }

      // Initialize a form derived from a mail template.
      $this->derive_form($form_state['template_name'], $form, $form_state);

    }
  	// Default to creating a newsletter with extra selectbox for templates.
    else {
      if (!$this->list) {
        drupal_set_message(t('No mail templates available, please create a mail template first.'));
        $form['buttons']['next']['#access'] = FALSE;
        return;
      }
      $this->default_edit_form($form, $form_state);
  	}

  }

  /**
   * Implements ctools_export_ui::edit_form_basic_validate().
   * Validate submission of the newsletter settings form.
   */
  function edit_form_basic_validate($form, &$form_state) {

    parent::edit_form_validate($form, $form_state);

    // 'add' is a pre-reserved machine name
    if ($form_state['values']['name'] == 'add') {
      form_set_error('name', t("'add' can't be used as a title"));
    }

  }

  /**
   * Implements ctools_export_ui::edit_form_basic_submit().
   * Submit the newsletter settings form.
   */
  function edit_form_basic_submit($form, &$form_state) {

    parent::edit_form_submit($form, $form_state);

    // When new template is selected, render from source.
    if ($form_state['values']['mail_template_name'] != $form_state['values']['old_template_name']) {
      $display = $this->get_display_from_template($form_state['values']['mail_template_name']);
      $form_state['item']->display = $display;
      $form_state['display'] = &$form_state['item']->display;
    }

  }

  /**
   * Implements ctools_export_ui::edit_form_content().
   * Step 2 of wizard: Choose the content.
   */
  function edit_form_content(&$form, &$form_state) {
    parent::edit_form_content($form, $form_state);
  }

  /**
   * Fetch all mail templates.
   */
  private function get_mail_templates() {
    if (empty($this->list)) {
      $this->list = mchammer_mail_templates_list();
    }
    return $this->list;
  }

  /**
   * Gets the display for a template name.
   */
  private function get_display_from_template($template_name) {
    $mailtemplate_ui = new mchammer_mail_template_ui();
    return $mailtemplate_ui->create_newsletter($template_name);
  }

  /**
   * Derives a newsletter from a given template argument.
   */
  private function derive_form($template_name, &$form, &$form_state) {

    ctools_include('content');

    // Load the original mail template
    $template = mchammer_mail_template_load($template_name);

    // Get the basic edit form
    parent::edit_form($form, $form_state);

    $form['category'] = array(
      '#type' => 'hidden',
      '#default_value' => $template->category,
    );
    $form['mail_template_name'] = array(
      '#type' => 'hidden',
      '#default_value' => $template->name,
    );
    $form['old_template_name'] = array(
      '#type' => 'hidden',
      '#value' => $form_state['item']->mail_template_name,
    );

    $languages = $this->language_list();
    if (count($languages) > 1) {
      $form['language'] = array(
        '#type' => 'select',
        '#title' => t('Language'),
        '#default_value' => (isset($form_state['item']->language) ? $form_state['item']->language : language_default('language')),
        '#options' => $languages,
      );
    }
    else {
      $form['language'] = array(
        '#type' => 'hidden',
        '#value' => language_default('language'),
      );
    }

  }

  /**
   * Creates a form to add or edit basic newsletter information.
   */
  private function default_edit_form(&$form, &$form_state) {

    ctools_include('export');
    $options = array();
    foreach (ctools_export_load_object('mchammer_mail_templates', 'all') as $name => $option) {
      $options[$name] = $option->admin_title;
    }

    if (empty($options)) {
      return;
    }

    // Get the basic edit form
    parent::edit_form($form, $form_state);

    $form['mail_template_name'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $form_state['item']->mail_template_name,
      '#title' => t('Mail template'),
      '#description' => t('Mail template this newsletter should be derived from'),
      '#required' => TRUE,
    );

    $form['title']['#title'] = t('Title');
    $form['title']['#description'] = t('The title for this newsletter template.');

    $form['old_template_name'] = array(
      '#type' => 'hidden',
      '#value' => $form_state['item']->mail_template_name,
    );

    $languages = $this->language_list();
    if (count($languages) > 1) {
      $form['language'] = array(
        '#type' => 'select',
        '#title' => t('Language'),
        '#default_value' => (isset($form_state['item']->language) ? $form_state['item']->language : language_default('language')),
        '#options' => $languages,
      );
    }
    else {
      $form['language'] = array(
        '#type' => 'hidden',
        '#value' => language_default('language'),
      );
    }

  }

  /**
   * Return the list of available languages.
   */
  private function language_list() {

    $language_list = language_list('enabled');
    $languages = array();
    foreach ($language_list[1] as $lang) {
      $languages[$lang->language] = t($lang->name);
    }

    return $languages;

  }

}