<?php

/**
 * @file
 * General ui class.
 */
abstract class mchammer_ui extends ctools_export_ui {

  protected $cacheKeyPrefix = 'mchammer';
  protected $rendererHandler = 'mchammer';

  /**
   * Implements ctools_export_ui::edit_cache_get_key().
   * Figure out what the cache key is for this object.
   */
  function edit_cache_get_key($item, $op) {
    $export_key = $this->plugin['export']['key'];
    if ($op == 'edit') {
      $cache_key = $this->cacheKeyPrefix . ':' . $item->{$this->plugin['export']['key']};
    }
    else {
      $cache_key = $this->cacheKeyPrefix . ":::$op";
    }
    
    return $cache_key;
  }

  /**
   * @overrride
   * Called to save the final product from the edit form.
   * @see ctools_export_ui::edit_save_form.
   */
  function edit_save_form($form_state) {
    if (!empty($form_state['original name']) && !empty($form_state['form type']) && $form_state['form type'] == 'clone') {
      $form_state['item']->formType = $form_state['form type'];
      $form_state['item']->originalName = $form_state['original name'];
    }
    return parent::edit_save_form($form_state);
  }

  /**
   * Perform a final validation check before allowing the form to be
   * finished.
   */
  function edit_finish_validate(&$form, &$form_state) {

    if ($form_state['form type'] == 'clone') {
      $export_key = $this->plugin['export']['key'];
      $form_state['item']->{$export_key} = $form_state['values'][$export_key];
    }
    
    parent::edit_finish_validate($form, $form_state);
  }

  /**
   * Implements ctools_export_ui::edit_form_content().
   * Step 2 of wizard: Choose the content.
   */
  function edit_form_content(&$form, &$form_state) {

    ctools_include('ajax');
    ctools_include('plugins', 'panels');
    ctools_include('display-edit', 'panels');

    // If we are cloning an item, we MUST have this cached for this to work,
    // so make sure:
    if ($form_state['form type'] == 'clone' && empty($form_state['item']->export_ui_item_is_cached)) {
      $this->edit_cache_set($form_state['item'], 'clone');
    }

    $cache_key = $this->edit_cache_get_key($form_state['item'], $form_state['form type']);
    $cache = panels_edit_cache_get('mchammer:' . $cache_key);

    // Start our own renderer to render the panes in the admin UI.
    $form_state['renderer'] = panels_get_renderer_handler($this->rendererHandler, $cache->display);
    if (!empty($form_state['item']->mail_template_name)) {
      $form_state['renderer']->mail_template_name = $form_state['item']->mail_template_name;
    }

    $form_state['renderer']->cache = &$cache;

    $form_state['display'] = &$cache->display;
    $form_state['content_types'] = $cache->content_types;
    // Tell the Panels form not to display buttons.
    $form_state['no buttons'] = TRUE;
    $form_state['display_title'] = FALSE;
    $form_state['no preview'] = TRUE;

    $form = panels_edit_display_form($form, $form_state);
    $form['#tree'] = TRUE;

      // Add template variables settings
    $layout = panels_get_layout($cache->display->layout);
    if (isset($layout['variables'])) {

      $form['variables'] = array(
        '#title' => t('Template variables'),
        '#type' => 'fieldset',
      );
      
      $add_imce = FALSE;

      // Loop through the variables and create a form field for the variable.
      foreach ($layout['variables'] as $variable_key => $variable) {

        if (isset($variable['group'])) {
          $category = strtolower($variable['group']);

          if (!isset($form['variables'][$category])) {
            $form['variables'][$category] = array(
              '#type' => 'fieldset',
              '#title' => $variable['group'],
              '#collapsible' => TRUE,
              '#collapsed' => TRUE,
            );
          }

          $element = & $form['variables'][$category];
        }
        else {
          $element = & $form['variables'];
        }
        $element[$variable_key] = array(
          '#type' => $variable['type'],
          '#title' => $variable['title'],
        );

        // Input format support.
        if ($variable['type'] == 'text_format') {
          $element[$variable_key]['#base_type'] = 'textarea';


          $default = isset($cache->display->panel_settings['variables'][$variable_key]) ? $cache->display->panel_settings['variables'][$variable_key] : NULL;
          if (!is_array($default)) {
            // Get the default input format (first in the list).
            global $user;
            $formats = filter_formats($user);
            $format = array_shift($formats);
            $default = array('format' => $format->format, 'value' => $default);
          }

          $element[$variable_key]['#format'] = $default['format'];
          $element[$variable_key]['#default_value'] = $default['value'];
        }
        else {
          $element[$variable_key]['#default_value'] = isset($cache->display->panel_settings['variables'][$variable_key]) ? $cache->display->panel_settings['variables'][$variable_key] : NULL;
        }

        // Check if imce is requested for this field.
        if (!empty($variable['imce'])) {
          if (module_exists('imce')) {
            $add_imce = TRUE;
            $name = isset($variable['group']) ? 'edit-variables-' . $category . '-' . str_replace('_', '-', $variable_key) . '-IMCE-image' : 'edit-variables-' . str_replace('_', '-', $variable_key) . '-IMCE-image';
            $attributes =  array('name' => $name, 'class' => array('mchammer-imce'));
            $element[$variable_key]['#description'] = t('Insert !image', array('!image' => l(t('image'), 'imce', array('attributes' => $attributes))));
          }
          else {
            $element[$variable_key]['#description'] = t('Enter a valid path to the image. You might want to install IMCE module as this allows to pick an image from the server.');
          }
        }

      }

      // Add imce js
      if ($add_imce) {
        $form['#attached']['js'][] = drupal_get_path('module', 'mchammer') . '/js/mchammer_imce.js';
      }

    }

    // Remove the update button
    if (isset($form['buttons']['next'])) {
      unset($form['buttons']['next']);
    }

    $id = $form_state['form type'] == 'add' ? 'new' : $form_state['item']->name;

    $attribs = array('attributes' => array('class' => 'button', 'target' => '_blank'));
    if (isset($form_state['item']->language)) {
      $languages = language_list();
      if (isset($languages[$form_state['item']->language])) {
        $attribs['language'] = $languages[$form_state['item']->language];
      }
    }

    $form['buttons']['preview'] = array(
      '#markup' => l(t('Preview'), $this->mchammerType . '/' . $id, $attribs),
      '#id' => 'panels-preview-button',
    );

    // Make sure the theme will work since our form id is different.
    $form['#theme'] = 'panels_edit_display_form';

  }

  /**
   * Implements ctools_export_ui::edit_form_content_submit().
   * Save the display.
   */
  function edit_form_content_submit(&$form, &$form_state) {

    panels_edit_display_form_submit($form, $form_state);

    // Add and save the MCHammer variables.
    $form_state['display']->variables = array();

    // Add template variables settings.
    $layout = panels_get_layout($form_state['display']->layout);
    if (isset($layout['variables']) && isset($form_state['values']['variables'])) {
      // Loop through the variables and create a form field for the variable.
      foreach ($layout['variables'] as $variable_key => $variable) {
        if (isset($variable['group'])) {
          $form_state['display']->panel_settings['variables'][$variable_key] = $form_state['values']['variables'][strtolower($variable['group'])][$variable_key];
        }
        else {
          $form_state['display']->panel_settings['variables'][$variable_key] = $form_state['values']['variables'][$variable_key];
        }
      }
    }

    $form_state['item']->display = $form_state['display'];
  }

}