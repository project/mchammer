(function($) {

/**
 * Add listener on all imce links for mchammer.
 */
Drupal.behaviors.mchammerImce = {attach: function(context, settings) {
  
  $('a.mchammer-imce', context).not('.processed').addClass('processed').click(function() {

    var i = this.name.indexOf('-IMCE-');
    var field_name = this.name.substr(0, i);
    window.open(this.href + '?app=mchammer|url@' + encodeURIComponent(field_name), '', 'width=760,height=560,resizable=1');
    return false;
    
  });
  
}};

})(jQuery);