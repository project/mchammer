<?php
/**
 * @file
 * Administration pages for MCHammer mail templates and newsletters.
 */

/**
 * General settings form for mc hammer.s
 */
function mchammer_settings_form() {

  $form = array();

  ctools_include('content');
  $content_types = ctools_content_get_available_types();
  $renderer = new panels_renderer_editor();
  $categories = $renderer->get_categories($content_types);

  $options = array();
  foreach ($categories as $key => $category) {

    // Root subcategories
    if ($key == 'root') {
      foreach ($category['content'] as $sub_key => $sub_category) {
        $options[$sub_key] = $sub_category['title'];
      }
    }
    // Tabs
    else {
      $options[$key] = $category['title'];
    }

  }

  $form['mchammer_panel_categories'] = array(
    '#title' => t('Panel categories that are enabled for newsletters'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => variable_get('mchammer_panel_categories', array()),
    '#description' => t("Categories that are not selected, will not be visible in the 'Add content' from panels"),
  );

  $entity_info = entity_get_info('node');
  foreach ($entity_info['view modes'] as $name => $data) {
    $view_modes_options[$name] = $data['label'];
  }
  $form['mchammer_view_modes'] = array(
    '#title' => t('View modes for Newsletters'),
    '#type' => 'checkboxes',
    '#options' => $view_modes_options,
    '#default_value' => variable_get('mchammer_view_modes', array()),
    '#description' => t("Enable all view modes you want to use while creating Newsletters."),
  );

  return system_settings_form($form);

}