<?php

/**
 * @file
 * Hooks provided by the mchammer module.
 *
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Respond to updates to a newsletter.
 *
 * This hook is invoked from mchammer_ctools_crud_save() after the newsletter is updated in the
 * mchammer_newsletters table in the database.
 *
 * @param $object
 *   The newsletter that is being updated.
 */
function hook_mchammer_newsletter_update($object) {
  db_update('mytable')
    ->fields(array('extra' => 'extra'))
    ->condition('mnid', $object->id)
    ->execute();
}

/**
 * Respond to updates to a mail template.
 *
 * This hook is invoked from mchammer_ctools_crud_save() after the mailtemplate is updated in the
 * mchammer_mail_templates table in the database.
 *
 * @param $object
 *   The mail template that is being updated.
 */
function hook_mchammer_mail_template_update($object) {
  db_update('mytable')
    ->fields(array('extra' => 'update'))
    ->condition('mtid', $object->id)
    ->execute();
}

/**
 * Respond to the creation of a newsletter.
 *
 * This hook is invoked from mchammer_ctools_crud_save() after the newsletter is added into the
 * mchammer_newsletters table in the database.
 *
 * @param $object
 *   The newsletter that is being added.
 */
function hook_mchammer_newsletter_insert($object) {
  db_insert('mytable')
    ->fields(array('mnid' => $object->id))
    ->execute();
}

/**
 * Respond to creation of a mail template.
 *
 * This hook is invoked from mchammer_ctools_crud_save() after the mailtemplate is added into the
 * mchammer_mail_templates table in the database.
 *
 * @param $object
 *   The mail template that is being updated.
 */
function hook_mchammer_mail_template_insert($object) {
  db_insert('mytable')
    ->fields(array('mnid' => $object->id))
    ->execute();
}

/**
 * @} End of "addtogroup utility functions".
 */

